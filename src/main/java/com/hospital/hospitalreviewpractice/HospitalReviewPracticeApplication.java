package com.hospital.hospitalreviewpractice;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class HospitalReviewPracticeApplication {

	public static void main(String[] args) {
		SpringApplication.run(HospitalReviewPracticeApplication.class, args);
	}

}
